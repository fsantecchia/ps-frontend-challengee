import { LikesSection } from '../components/LikesSection/LikesSection'
import './App.css';

const USER_ID = 'user1';
const LIKE_ID = Date.now().toString();

export const App = () => {
  return (
    <div className="app">
      <LikesSection loggedInUserId={USER_ID} likeId={LIKE_ID} />
      <LikesSection loggedInUserId={USER_ID + 1} likeId={LIKE_ID} debugInitialCount={999} />
      <LikesSection loggedInUserId={USER_ID} likeId={LIKE_ID + 2} debugInitialCount={1459} />
      <LikesSection loggedInUserId={USER_ID} likeId="persistent" />
    </div>
  );
}
