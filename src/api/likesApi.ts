const API_URL = 'http://localhost:3001/api/v1';

export const fetchLikesCount = (likeId: string): Promise<number> => {
  const url = `${API_URL}/like/${likeId}/count`;

  return fetch(url)
    .then((response) => response.json())
    .then((response) => response.data)
}

export const fetchHasUserLiked = (likeId: string, userId: string): Promise<boolean> => {
  const url = `${API_URL}/like/${likeId}/user/${userId}`;

  return fetch(url)
    .then((response) => response.json())
    .then((response) => response.data)
}

export const toggleLike = (likeId: string, userId: string, action: 'add' | 'remove'): Promise<any> => {
  return fetch(`${API_URL}/like/${action}`, {
    method: 'POST',
    body: JSON.stringify({ 
      likeId, 
      userId 
    }),
    headers: {
      'Content-Type': 'application/json'
    }
  })
}