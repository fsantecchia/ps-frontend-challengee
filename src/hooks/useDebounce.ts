import { useEffect, useRef, useMemo } from 'react';

export const useDebounce = (callback: () => void, delay: number) => {
  const callbackRef = useRef(callback);
  const timeoutId = useRef<any>(null);

  useEffect(() => {
    callbackRef.current = callback;
  }, [callback]);

  const debounced = () => {
    if (timeoutId.current) {
      clearTimeout(timeoutId.current)
    }

    timeoutId.current = setTimeout(callbackRef.current || callback, delay);
  }

  return useMemo(() => debounced, []);
  };