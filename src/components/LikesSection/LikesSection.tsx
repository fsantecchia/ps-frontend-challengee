import { useEffect, useState } from 'react';

import { LikeIcon } from './LikeIcon/LikeIcon';
import { LikesCount } from './LikesCount/LikesCount';
import { fetchLikesCount, fetchHasUserLiked, toggleLike } from '../../api/likesApi';
import { useDebounce } from '../../hooks/useDebounce';

import './LikesSection.css';

const DEBOUNCE_DELAY = 300;

type Props = {
  likeId: string;
  loggedInUserId: string;

  // only for debugging propuse 
  debugInitialCount?: number;
}

export const LikesSection = (props: Props) => {
  const [likesCount, setLikesCount] = useState(0);
  const [hasLoggedInUserLiked, setHasLoggedInUserLiked] = useState(false);
  const [isLoading, setIsLoading] = useState(true)

  // fetch initial data
  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);

      // fetch data in parallel
      const [fetchLikesCountResponse, fetchHasUserLikedResponse] = await Promise.all([
        fetchLikesCount(props.likeId),
        fetchHasUserLiked(props.likeId, props.loggedInUserId)
      ]);

      setLikesCount(fetchLikesCountResponse + (props.debugInitialCount ?? 0));
      setHasLoggedInUserLiked(fetchHasUserLikedResponse);
      setIsLoading(false);
    }

    fetchData();
  }, [])

  const storeNewLikeValue = useDebounce(() => {
    toggleLike(props.likeId, props.loggedInUserId, hasLoggedInUserLiked ? 'remove' : 'add');
  }, DEBOUNCE_DELAY);

  const handleLikeButton = () => {
    // update UI optimistically
    setLikesCount(hasLoggedInUserLiked ? likesCount - 1 : likesCount + 1);
    setHasLoggedInUserLiked(!hasLoggedInUserLiked);

    // only store last like value after DEBOUNCE_DELAY
    storeNewLikeValue();
  };

  if (isLoading) {
    return <div>Loading...'</div>;
  }

  return (
    <div className="likes-section">
      <button
        onClick={handleLikeButton}
        className="like-button"
      >
        <LikeIcon liked={hasLoggedInUserLiked} />
      </button>
      <LikesCount count={likesCount} />
    </div>
  );
}
