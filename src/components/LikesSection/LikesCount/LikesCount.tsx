type Props = {
  count: number;
}

const formatter = Intl.NumberFormat('en-US', {
  notation: "compact",
  maximumFractionDigits: 1
});

export const LikesCount = (props: Props) => { 
  return (
    <span>{formatter.format(props.count)}</span>
  );
}
